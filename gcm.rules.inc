<?php
/**
 * @file
 * The rules hook and actions
 *
 * Define each action, messages to users, etc.
 */

/**
 * Implementation of hook_rules_action_info().
 */
function gcm_rules_action_info() {
  return array(
    'gcm_action_send_message' => array(
      'label' => t('Send GCM message'),
      'module' => 'Google Cloud Messaging',
      'eval input' => array('tokens', 'keyValue'),
    ),
  );
}


function gcm_action_send_message($settings) {

    // Get form values   
    $tokens = strip_tags($settings['tokens']);
    $keyValues = $settings['keyValue'];

    $gcm_message = array();
    foreach (explode(",", $keyValues) as $keyValue) {
      $temp = explode("=", $keyValue);
      $key = $temp[0];
      $value = $temp[1];
      if (isset($key) && isset($value)) {
        $gcm_message[$key] = $value;
      }
    }
    gcm_send_message(array_unique(explode(',', $tokens)), $gcm_message);
}
